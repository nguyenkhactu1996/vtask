import {useNavigate} from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();

  const handleGoToSingUp = () => {
    navigate("/auth/signup")
  }

  return (
      <div className={"row"}>
        <div className={"col-2"}>
          Login Page
        </div>
      </div>
  )
}

export default Login