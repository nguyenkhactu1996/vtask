import {Outlet} from "react-router-dom";
import welcomeLogo from '../../asset/img/welcome-logo.png'
import './auth.css'

const Auth = () => {
  return (
      <div className={"container"}>
        <div className={"welcome-logo-container"}>
          <img className={"welcome-logo"} src={welcomeLogo} alt=""/>
        </div>
        <Outlet />
      </div>
  )
}

export default Auth