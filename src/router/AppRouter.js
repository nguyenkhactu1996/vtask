import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import RootLayout from "./RootLayout";
import Login from "../pages/Auth/components/Login";
import SignUp from "../pages/Auth/components/SignUp";
import HomePage from "../pages/App/HomePage";
import Auth from "../pages/Auth";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout/>,
    children: [
      {
        path: "auth",
        element: <Auth/>,
        children: [
          {path: "login", element: <Login/>},
          {path: "signup", element: <SignUp/>}
        ]
      },
      {
        index: true,
        element: <HomePage/>
      }
    ]
  },
]);

const AppRouter = () => {
  return <RouterProvider router={router}/>
}

export default AppRouter